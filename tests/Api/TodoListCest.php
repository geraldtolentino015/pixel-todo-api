<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class TodoListCest
{
    public function iShouldGetData(ApiTester $I)
	{
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/localhost:4000/API.php');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldGetSpecificData(ApiTester $I)
	{
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/localhost:4000/API.php', ['id' => 1]);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldInsertData(ApiTester $I)
	{
	    $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/localhost:4000/API.php/',
            ['id' => 10,
            'task_title' => 'Task 1',
	        'task_name' => 'Write Code',
	        'time' => '2024-02-18 10:04:51',
	        'status' => 'Inprogress']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldUpdateData(ApiTester $I)
	{
	    $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/localhost:4000/API.php/1',
            ['task_title' => 'Task 1',
	        'task_name' => 'Write Code',
	        'time' => '2024-02-20 17:24:56',
	        'status' => 'Done']);
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }

    public function iShouldDeleteData(ApiTester $I)
	{
	    $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/localhost:4000/API.php/10');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'success']);
    }
}
