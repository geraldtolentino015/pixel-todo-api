<?php


namespace Tests\Api;

use Tests\Support\ApiTester;

class ToDoListFailCest
{
    public function iShouldNotGetSpecificData(ApiTester $I)
	{
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGet('/localhost:4000/API.php', ['id' => 30]);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'failed']);
    }

    public function iShouldNotInsertData(ApiTester $I)
	{
	    $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/localhost:4000/API.php/',
            ['id' => 1,
            'task_title' => 'Task 1',
	        'task_name' => 'Write Code',
	        'time' => '2024-02-18 10:04:51',
	        'status' => 'Inprogress']);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'failed']);
    }

    public function iShouldNotUpdateData(ApiTester $I)
	{
	    $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPut('/localhost:4000/API.php/30',
            ['task_title' => 'Task 1',
	        'task_name' => 'Write Code',
	        'time' => '2024-02-20 17:24:56',
	        'status' => 'Done']);
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'failed']);
    }

    public function iShouldNotDeleteData(ApiTester $I)
	{
	    $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendDelete('/localhost:4000/API.php/30');
        $I->seeResponseCodeIs(400);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['status' => 'failed']);
    }
}
