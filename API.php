<?php
use function PHPSTORM_META\type;

  header("Access-Control-Allow-Origin: *");
  header("Access-Control-Allow-Credentials: true");
  header("Access-Control-Allow-Methods: POST, GET, PUT, DELETE");
  header("Access-Control-Allow-Headers: Content-Type");

  require_once('MysqliDb.php');

  class API {
    private $db;

    public function __construct()
    {
      $this->db = new MysqliDb('localhost', 'root', '', 'pixel');
    }

    /**
     * HTTP GET Request
     *
     * @param $payload
     */
    public function httpGet($payload)
    {
      // start your code here!!
      if (!is_array($payload)) {
        $this->errorResponse('GET', 'Payload must be an array');
        return;
      } else if ($payload != null) {
        $this->db->where('id', $payload['id']);
        $todos = $this->db->get("tbl_to_do_list");
        if ($todos) {
          $this->successResponse('GET', $todos);
          return;
        } else {
          $this->errorResponse('GET', 'ToDo does not exist');
          return;
        }
      }

      $todos = $this->db->get("tbl_to_do_list");

      if ($todos) {
        $this->successResponse('GET', $todos);
      } else {
        $this->errorResponse('GET', 'Failed to Fetch Request');
      }
    }

    /**
     * HTTP POST Request
     *
     * @param $payload
     */
    public function httpPost($payload)
    {     
      if(!is_array($payload) || $payload == null) {
        $this->errorResponse('POST', 'Payload must be an array and not be empty');
        return;
      }

      $data = Array ('id' => $payload['id'],
                    'task_title' => $payload['task_title'],
                    'task_name' => $payload['task_name'],
                    'time' => $payload['time'],
                    'status' => $payload['status']);
      
      $id = null;
      try {
        $id = $this->db->insert('tbl_to_do_list', $data);
      }
      catch (Exception $e) {
        $this->errorResponse('POST', 'Failed to Insert Data');
        return;
      }
 
      if ($id) {
        $this->successResponse('POST', $data);
      } else {
        $this->errorResponse('POST', 'Failed to Insert Data');
      }
    }

    /**
     * HTTP PUT Request
     *
     * @param $id
     * @param $payload
     */
    public function httpPut($id, $payload)
    {
      if (!is_numeric($id)) {
        $this->errorResponse('PUT', 'ID should not be empty');
        return;
      } else if ($payload == null) {
        $this->successResponse('PUT', 'Payload should not be empty"');
        return;
      }

      $check = $this->idCheck('PUT', $id);
      if (!$check) { return; }

      $data = Array ('id' => $id,
                    'task_title' => $payload['task_title'],
                    'task_name' => $payload['task_name'],
                    'time' => $payload['time'],
                    'status' => $payload['status']);
      
      $this->db->where('id', $id);
      $isUpdated = $this->db->update('tbl_to_do_list', $data);

      if ($isUpdated) {
        $this->successResponse('PUT', $data);
      } else {
        $this->errorResponse('PUT', 'Failed to Update Data');
      }
    }

    /**
     * HTTP DELETE Request
     *
     * @param $id
     * @param $payload
     */
    public function httpDelete($id)
    {
      if (!is_numeric($id)) {
        $this->errorResponse('DELETE', 'ID should not be emtpy');
        return;
      }

      $data = $this->idCheck('DELETE', $id);
      if (!$data) { return; } 

      $this->db->where('id', $id);
      $isDelete = $this->db->delete('tbl_to_do_list');

      if ($isDelete) {
        $this->successResponse('DELETE', $data);
      } else {
        $this->errorResponse('DELETE', 'Failed to Delete Data');
      }
    }

    private function successResponse($method, $data = []) {
      http_response_code(200);
      echo json_encode(array(
        'method' => $method,
        'status' => 'success',
        'data' => $data,
      ));
    }

    private function errorResponse($method, $message) {
      http_response_code(400);
      echo json_encode(array(
          'method' => $method,
          'status' => 'failed',
          'message' => $message
      ));
    }

    private function idCheck($method, $id) {
      $this->db->where('id', $id);
      $toDoExist = $this->db->getOne('tbl_to_do_list');

      if (!$toDoExist) {
        $this->errorResponse($method, 'ToDo does not exist');
        return false;
      }

      return $toDoExist;
    }
  }

  //Identifier if what type of request
  $request_method = $_SERVER['REQUEST_METHOD'];
  
  // For GET,POST,PUT & DELETE Request
  if ($request_method === 'GET') {
    $received_data = $_GET;
  } else if ($request_method === 'PUT' || $request_method === 'DELETE') {
    $request_uri = $_SERVER['REQUEST_URI'];
    $ids = null;
    $exploded_request_uri = array_values(explode("/", $request_uri));
    $last_index = count($exploded_request_uri) - 1;
    $ids = $exploded_request_uri[$last_index];
    $received_data = json_decode(file_get_contents('php://input'), true);
  } else if ($request_method === 'POST') {
    $received_data = json_decode(file_get_contents('php://input'), true);
  }

  $api = new API;

 //Checking if what type of request and designating to specific functions
  switch ($request_method) {
      case 'GET':
          $api->httpGet($received_data);
          break;
      case 'POST':
          $api->httpPost($received_data);
          break;
      case 'PUT':
          $api->httpPut($ids, $received_data);
          break;
      case 'DELETE':
          $api->httpDelete($ids);
          break;
  }
